from django.shortcuts import render, get_object_or_404, get_list_or_404
from .models import Liste 
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone

def index(request):
    latest_listen = Liste.objects.order_by('pub_date')
    to_do_listen = Liste.objects.filter(liste_status=1)
    done_listen = Liste.objects.filter(liste_status=2)
    canceled_listen = Liste.objects.filter(liste_status=3)
    context = {'latest_listen':latest_listen, 'to_do_listen':to_do_listen, 'done_listen':done_listen, 'canceled_listen':canceled_listen}
    return render(request, 'to_do_app/index.html', context)


def detail(request, liste_title):
    liste =  Liste.objects.get(liste_title=liste_title)
    return render(request, 'to_do_app/detail.html', {'liste':liste})


def new_detail(request, liste_title):
    liste =  Liste.objects.get(liste_title=liste_title)
    liste.liste_status = request.POST.get("choice")
    liste.save(update_fields=["liste_status"])
    return HttpResponseRedirect(reverse('to_do_app:detail', args=(liste.liste_title,)))


def new_list(request):
    print(request.POST)
    new_liste = Liste()
    new_liste.liste_title = request.POST.get("title")
    new_liste.liste_text = request.POST.get("text")
    new_liste.pub_date = timezone.now()
    new_liste.liste_status = request.POST.get("choice")
    new_liste.save()
    return HttpResponseRedirect(reverse('to_do_app:index'))
