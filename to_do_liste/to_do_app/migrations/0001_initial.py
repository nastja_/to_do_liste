# Generated by Django 2.2.5 on 2020-10-26 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Liste',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('liste_title', models.CharField(max_length=50, verbose_name='name of list')),
                ('liste_text', models.TextField(max_length=1000, verbose_name='text of list')),
                ('pub_date', models.DateTimeField(verbose_name='date of list')),
                ('liste_status', models.IntegerField(choices=[(1, 'Todo'), (2, 'Done'), (3, 'Canceled')])),
            ],
        ),
    ]
