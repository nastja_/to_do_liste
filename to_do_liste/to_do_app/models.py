from django.db import models
import datetime

class Liste(models.Model):
    liste_title = models.CharField('name of list', max_length = 50)
    liste_text = models.TextField('text of list', max_length = 1000)
    pub_date = models.DateTimeField('date of list')
    liste_status = models.IntegerField(choices=[[1,'Todo'], [2, 'Done'], [3, 'Canceled']])
    
    def __str__(self):
      return self.liste_title


# Create your models here.
